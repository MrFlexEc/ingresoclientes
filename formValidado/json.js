function validar(){
    //Capturamos los elementos principales el formulario
    valorCedu = document.getElementById('cedula').value;
    valorFName = document.getElementById('fname').value;
    valorLName = document.getElementById('lname').value;
    valorTelef = document.getElementById('Telefono').value;
    valorEmail = document.getElementById('correo').value;

    //Hacemos la respectiva validacion de si son campos nulos o vacios con una alerta si los campos
    //estan vacios o nulos
    if(valorCedu == null || valorCedu.length ==0){
        alert('El campo CEDULA no puede estar vacio');
        return false;  
    }
    if(valorFName == null || valorFName.length ==0){
        alert('El campo NOMBRE no puede estar vacio');
        return false;      
    }
    if(valorLName == null || valorLName.length ==0){
        alert('El campo APELLIDO no puede estar vacio');
        return false;    
    }
    if(valorTelef == null || valorTelef == 0){
        alert('El campo TELEFONO no puede estar vacio');
        //adicional hacemos la validacion de que sea un campo con valor numerico
        if( isNaN(valorTelef) ){
            alert('El valor no es numerico');
            return false;
        }
        return false;  
    }
    //Devolvemos una alerta cuando todos los datos han sido validados
    alert("Sus datos han sido registrados");
    return true;
    
}