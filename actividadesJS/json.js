/* Creamos una funcion con un parametro*/
function transDecimal(num1){
    //hacemos un parseInt al valor recogido por parametro, para obtener el valor en octal
    var datito = parseInt(num1, 8)
    alert(datito);
}

//Creamos un metodo con 2 parametros, que seran los dos numeros con los que realizaremos las operaciones
function operaciones(int1, int2){
    //hacemos un parseInt a los dos valores dado que estan en string
    var int11 = parseInt(int1); 
    var int12 = parseInt(int2);

    //hacemos la respectivas operaciones: SUMA | RESTA | MULTIPLICACION | DIVISION
    var suma = int11 + int12;
    var resta = int11 - int12;
    var multi = int11 * int12;
    var div = int11 / int12;

    alert("El resultado de la suma de los dos numeros es: " + suma );
    alert("El resultado de la resta de los dos numeros es: " + resta );
    alert("El resultado del producto de los dos numeros es: " + multi );
    alert("El resultado de la division de los dos numeros es: " + div );
    
}


function mostrarMeses(){
    //Creamos un array que en su interior tengan los meses del anio
    var meses = new Array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

    //con un bucle for que tenga como condicion que el contador sea menor a la longitud del array, muestre por alerta, uno a uno
    //cada uno de los elementos del array
    for(i = 0; i < meses.length; i++){
        alert(meses[i]);
    }

}

function addProdu(){
    //creamos un constructor de objetos para los productos
    function Producto_alimenticio(code,name,price){
    //usamos el atributo THIS
    this.code=code;
    this.name=name;
    this.price=price;
    }
    //capturamos los elementos del HTML que estan en el INPUT
    var PRO_CODE = document.getElementById("PRO_CODE").value;
    var PRO_NAME = document.getElementById("PRO_NAME").value;
    var PRO_PRICE = document.getElementById("PRO_PRICE").value;
    //creamos una instancia de tipo PRODUCTO_ALIMENTICIO con sus respectivos parametros
    PRO_NEW= new Producto_alimenticio(PRO_CODE,PRO_NAME,PRO_PRICE);
    //hacemos un PUSH a la lista, y que el valor sea el objeto instanciado
    Productos.push(PRO_NEW);
    //Mostramos por pantalla que el producto ha sido agregado a la lista
    alert("PRODUCTO AGREGADO");
}
    
//Creacion de una lista para almacenar los objetos
Productos=[];

function Mostrar(){    
    //Se llamada al metodo predeterminado FOREACH
    Productos.forEach(item => {
    alert("Codigo: " + item.code +"\n" +"Nombre: " + item.name + "\n"+"Precio: $" + item.price);
    });
}